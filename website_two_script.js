var dataURL = "api.capturedata.ie";

var pageAddress = document.URL;

var flightData = {};
var outboundDetails = {};
var returnDetails = {};

//check if the URL contains part of the string "britishairways"
if(pageAddress.indexOf("britishairways") > -1 ){
	outboundDetails["departureCity"] = document.getElementById("rsr_departureCity").value;
	outboundDetails["departureCityCode"] = document.getElementById("rsr_departureCode").value;
	outboundDetails["outboundDepartureDate"] = document.getElementById("rsr_departureDate").value;
	outboundDetails["outboundDepartureTime"] = document.getElementById("rsr_departureFlightTime").value;
	outboundDetails["outboundArrivalDate"] = document.getElementById("rsr_arrivalDate").value;
	outboundDetails["outboundArrivalTime"] = document.getElementById("rsr_arrivalTime").value;
	outboundDetails["outboundFlightNumber"] = document.getElementById("rsr_departureFlight").value;

	returnDetails["arrivalCity"] = document.getElementById("rsr_arrivalCity").value;
	returnDetails["arrivalCityCode"] = document.getElementById("rsr_arrivalCode").value;
	returnDetails["returnDepartureDate"] = document.getElementById("rsr_returnDate").value;
	returnDetails["returnDepartureTime"] = document.getElementById("rsr_returnFlightTime").value;
	returnDetails["returnArrivalDate"] = document.getElementById("rsr_returnArrivalDate").value;
	returnDetails["returnArrivalTime"] = document.getElementById("rsr_returnArrivalTime").value;
	returnDetails["returnFlightNumber"] = document.getElementById("rsr_returnFlight").value;

	flightData["outboundDetails"] = outboundDetails;
	flightData["returnDetails"] = returnDetails;
	
	sendData()
}

//check if the URL contains part of the string "lufthansa"
else if(pageAddress.indexOf("lufthansa") > -1 ){

	var flightDetailsOutbound = clientSideData.PAGE.PANELS.IRC_FLIGHTS.DATA.BOUND_1.LIST_SEGMENT_FLIGHT_DETAILS[0]
	outboundDetails["departureCity"] = flightDetailsOutbound["DEPARTURE_CITY_NAME"];
	outboundDetails["departureCityCode"] = flightDetailsOutbound["DEPARTURE_LOCATION_CODE"];
	outboundDetails["outboundDepartureDate"] = flightDetailsOutbound["DEPARTURE_DATE"];
	outboundDetails["outboundDepartureTime"] = flightDetailsOutbound["DEPARTURE_TIME"];
	outboundDetails["outboundArrivalDate"] = flightDetailsOutbound["ARRIVAL_DATE"];
	outboundDetails["outboundArrivalTime"] = flightDetailsOutbound["ARRIVAL_TIME"];
	var flightNumberOutbound = flightDetailsOutbound["AIRLINE_CODE"] + flightDetailsOutbound["FLIGHT_NUMBER"]
	outboundDetails["outboundFlightNumber"] = flightNumberOutbound;

	var flightDetailsReturn = clientSideData.PAGE.PANELS.IRC_FLIGHTS.DATA.BOUND_2.LIST_SEGMENT_FLIGHT_DETAILS[0]
	returnDetails["arrivalCity"] = flightDetailsReturn["DEPARTURE_CITY_NAME"];
	returnDetails["arrivalCityCode"] = flightDetailsReturn["DEPARTURE_LOCATION_CODE"];
	returnDetails["returnDepartureDate"] = flightDetailsReturn["DEPARTURE_DATE"];
	returnDetails["returnDepartureTime"] = flightDetailsReturn["DEPARTURE_TIME"];
	returnDetails["returnArrivalDate"] = flightDetailsReturn["ARRIVAL_DATE"];
	returnDetails["returnArrivalTime"] = flightDetailsReturn["ARRIVAL_TIME"];
	var flightNumberreturn = flightDetailsReturn["AIRLINE_CODE"] + flightDetailsReturn["FLIGHT_NUMBER"]
	returnDetails["returnFlightNumber"] = flightNumberreturn;

	flightData["outboundDetails"] = outboundDetails;
	flightData["returnDetails"] = returnDetails;
	
	sendData()
}

else{
	alert("This script will not run on this website")
}


function sendData() {
    var xhttp = new XMLHttpRequest();
	xhttp.open('POST', dataURL, true);

	//send data as a javascript object
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(flightData);

	//Convert to JSON so results are easier to read and print to console. 
	//Comment the above two lines and uncomment the two below if wish to send the data in JSON format
	flightDataJSON = JSON.stringify(flightData);
	console.log(flightDataJSON);

	//send data as JSON
	//xhttp.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
	//xhttp.send(flightDataJSON);
}


