var dataURL = "api.capturedata.ie";

var flightData = {};
var outboundDetails = {};
var returnDetails = {};

outboundDetails["departureCity"] = document.getElementById("rsr_departureCity").value;
outboundDetails["departureCityCode"] = document.getElementById("rsr_departureCode").value;
outboundDetails["outboundDepartureDate"] = document.getElementById("rsr_departureDate").value;
outboundDetails["outboundDepartureTime"] = document.getElementById("rsr_departureFlightTime").value;
outboundDetails["outboundArrivalDate"] = document.getElementById("rsr_arrivalDate").value;
outboundDetails["outboundArrivalTime"] = document.getElementById("rsr_arrivalTime").value;
outboundDetails["outboundFlightNumber"] = document.getElementById("rsr_departureFlight").value;

returnDetails["returnCity"] = document.getElementById("rsr_arrivalCity").value;
returnDetails["returnCityCode"] = document.getElementById("rsr_arrivalCode").value;
returnDetails["returnDepartureDate"] = document.getElementById("rsr_returnDate").value;
returnDetails["returnDepartureTime"] = document.getElementById("rsr_returnFlightTime").value;
returnDetails["returnArrivalDate"] = document.getElementById("rsr_returnArrivalDate").value;
returnDetails["returnArrivalTime"] = document.getElementById("rsr_returnArrivalTime").value;
returnDetails["returnFlightNumber"] = document.getElementById("rsr_returnFlight").value;

flightData["outboundDetails"] = outboundDetails;
flightData["returnDetails"] = returnDetails;


var xhttp = new XMLHttpRequest();
xhttp.open('POST', dataURL, true);

//send data as a javascript object
xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xhttp.send(flightData);

//Convert to JSON so results are easier to read and print to console. 
//Comment the above two lines and uncomment the two below if wish to send the data in JSON format
flightDataJSON = JSON.stringify(flightData);
console.log(flightDataJSON);

//send data as JSON
//xhttp.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
//xhttp.send(flightDataJSON);